<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

$betStatusDetails = getBetstatus($conn, "WHERE uid =?",array("uid"),array($uid),"s");

foreach ($betStatusDetails as $betDetails) {
	// echo $betDetails->getAmount()."<br>";
}
echo date('h:i a');
 ?>
