<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/submitDeposit.php" />
    <meta property="og:title" content="Submit Deposit| De Xin Guo Ji 德鑫国际" />
    <title>Submit Deposit| De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/submitDeposit.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">


<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">
    <h1 class="h1-title">Submit Deposit</h1> 
    
        <form   action="utilities/submitDepositFunction.php" method="POST">
            <div class="input50-div">
                <p class="input-title-p">Name</p>
                <input class="clean tele-input" type="text" id="deposit_name" name="deposit_name" value="<?php echo $userDetails->getUsername();?>" readonly>        
            </div> 

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Bank Name</p>
                <input class="clean tele-input"  type="text" placeholder="Bank Name" id="deposit_bank_name" name="deposit_bank_name" required>  
            </div>

            <div class="clear"></div>

            <div class="input50-div second-input50">
                <p class="input-title-p">Amount</p>
                <input class="clean tele-input"  type="text" placeholder="Amount" id="deposit_amount" name="deposit_amount" required>  
            </div>   

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Time</p>
                <input class="clean tele-input"  type="date" placeholder="Time" id="deposit_date" name="deposit_date">
                <input class="clean tele-input"  type="time" placeholder="Time" id="deposit_time" name="deposit_time">  
            </div>

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Reference</p>
                <input class="clean tele-input"  type="text" placeholder="Reference" id="deposit_reference" name="deposit_reference">  
            </div>

            <div class="clear"></div>

            <input class="clean tele-input"  type="hidden" id="deposit_uid" name="deposit_uid" value="<?php echo $userDetails->getUid();?>" readonly>

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="refereeButton">Submit</button>
        </form>

       
</div>

<style>
.account-li{
	color:#bf1b37;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>

<!-- <?php //include 'js.php'; ?> -->

</body>
</html>