<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];

        $fullname = rewrite($_POST["edit_fullname"]);
        $username = rewrite($_POST["edit_username"]);
        $email = rewrite($_POST["edit_email"]);
        $phone = rewrite($_POST["edit_phone"]);

        //   FOR DEBUGGING 
        // echo "<br>";
        // echo $fullname."<br>";
        // echo $register_email."<br>";
        // echo $register_contact."<br>";

        $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

        if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($fullname)
            {
                array_push($tableName,"full_name");
                array_push($tableValue,$fullname);
                $stringType .=  "s";
            }
            if($username)
            {
                array_push($tableName,"username");
                array_push($tableValue,$username);
                $stringType .=  "s";
            }
            if($email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$email);
                $stringType .=  "s";
            }
            if($phone)
            {
                array_push($tableName,"phone_no");
                array_push($tableValue,$phone);
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../editProfile.php?type=1');
                // echo "<script>alert('Update Profile success !');window.location='../editProfile.php'</script>";
                $_SESSION['messageType'] = 1;
                header('Location: ../editProfile.php?type=1');
            }
            else
            {
                // echo "fail";
                // echo "<script>alert('Fail to update profile!');window.location='../editProfile.php'</script>";
                $_SESSION['messageType'] = 1;
                header('Location: ../editProfile.php?type=2');
            }
        }
        else
        {
            // echo "gg";
            // echo "<script>alert('This user is not available ');window.location='../editProfile.php'</script>";
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=3');
        }

    }
else 
{
    header('Location: ../index.php');
}
?>
