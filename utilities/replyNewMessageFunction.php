<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

function submitMessage($conn,$uid,$message_uid,$replySMS,$userStatus,$adminStatus,$replyOne,$replyThree)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","reply_message","user_status","admin_status","reply_one","reply_three"),
     array($uid,$message_uid,$replySMS,$userStatus,$adminStatus,$replyOne,$replyThree),"sssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $message_uid = rewrite($_POST["message_uid"]);
    $message_uid = md5(uniqid());
    $uid = rewrite($_POST["sender_uid"]);
    // $uid = rewrite($_POST["sender_uid"]);
    // $message_details = rewrite($_POST["message_details"]);
    $replySMS = rewrite($_POST["message_details"]);
    $adminStatus = "REPLY";
    $userStatus = "GET";
    $updateMessageStatus = "NO";
    $replyOne = $message_uid;
    $replyThree = "NEW";

    // //for debugging
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $senderUID."<br>";
    // echo $message_details."<br>";

    if(submitMessage($conn,$uid,$message_uid,$replySMS,$userStatus,$adminStatus,$replyOne,$replyThree))
    {
        header('Location: ../adminViewMessage.php?type=1');
    }
    else
    {
        header('Location: ../adminViewMessage.php?type=4');
    }

}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

?>