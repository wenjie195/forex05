<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function makeDeposit($conn,$depositUid,$userUsername,$depositAmount,$adminUsername,$approvedTime)
{
    if(insertDynamicData($conn,"deposit",array("uid","username","amount","verify_by","verify_time"),
        array($depositUid,$userUsername,$depositAmount,$adminUsername,$approvedTime),"sssss") === null)
    {
        echo "unable to register";
    }
    else{    }
    return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $time = $dt->format('Y-m-d H:i:s');

    $uid = $_SESSION['uid'];
    $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $adminUsername = $adminDetails[0]->getUsername();
    $approvedTime = $time;

    $depositUid = md5(uniqid());
    $userUid = rewrite($_POST["user_uid"]);

    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
    $userUsername = $userDetails[0]->getUsername();

    $currentCredit = rewrite($_POST["current_credit"]);

    if ($currentCredit != "")
    {
         $userCurrentCredit = $currentCredit;
    }
    else
    {
         $userCurrentCredit = "0";
    }

    $previousDeposit = rewrite($_POST["total_deposit"]);
    if ($previousDeposit != "")
    {
        // $depositAmount = "0";
        $depositAmount = $previousDeposit;
    }
    else
    {
        // $depositAmount = $previousDeposit;
        $depositAmount = "0";
    }

    $depositAmount = rewrite($_POST["topup_amount"]);
    $newCreditAmount = $userCurrentCredit + $depositAmount;
    $totalDeposit = $previousDeposit + $depositAmount;

    // //for debugging
    // echo "<br>";
    // echo $adminUsername."<br>";
    // echo $depositUid."<br>";
    // echo $userUid."<br>";
    // echo $userCurrentCredit."<br>";
    // echo $depositAmount."<br>";
    // echo $newCreditAmount."<br>";
    // echo $totalDeposit."<br>";
    // echo $approvedTime."<br>";

    if(isset($_POST['top_up']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($totalDeposit || !$totalDeposit)
        {
            array_push($tableName,"deposit");
            array_push($tableValue,$totalDeposit);
            $stringType .=  "s";
        }   
        if($newCreditAmount || !$newCreditAmount)
        {
            array_push($tableName,"credit");
            array_push($tableValue,$newCreditAmount);
            $stringType .=  "s";
        }   

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        
        if($orderUpdated)
        {
            // echo "success<br>";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminShipping.php?type=11');
            if(makeDeposit($conn,$depositUid,$userUsername,$depositAmount,$adminUsername,$approvedTime))
            {
                echo "<script>alert('Deposit Recorded!');window.location='../adminMemberList.php'</script>";
            }
            else
            {
                echo "<script>alert('Unable to Record Reposit !');window.location='../adminMemberList.php'</script>";
            }
        }
        else
        {
            echo "fail";
        }
    }
    else
    {
        echo "dunno";
    }
}
else 
{
    header('Location: ../adminMemberList.php');
}

?>