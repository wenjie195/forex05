<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = rewrite($_POST["sender_uid"]);
    $uid = $senderUID;
    $message_uid = md5(uniqid());

    $receiveSMS = rewrite($_POST["message_details"]);
    $adminStatus = "GET";
    $userStatus = "SENT";
    $updateMessageStatus = "YES";
    // $adminStatusOld = "UPTODATE";
    // $userStatusOld = "UPTODATE";
    $replyOne = $message_uid;

    $replyThree = "NEW";
    $previousMessageStatus = "OLD";

     //     //for debugging
     //     echo "<br>";
     //     echo $uid."<br>";
     //     echo $message_uid."<br>";
     //     echo $receiveSMS."<br>";
     //     echo $adminStatus."<br>";
     //     echo $userStatus."<br>";

     if(isset($_POST['sent_sms']))
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database"; 
          if($updateMessageStatus)
          {
               array_push($tableName,"message");
               array_push($tableValue,$updateMessageStatus);
               $stringType .=  "s";
          } 
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($messageStatusInUser)
          {
               // header('Location: ../viewMessage.php?type=1');

               if(isset($_POST['sent_sms']))
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database"; 
                    if($previousMessageStatus)
                    {
                         array_push($tableName,"reply_three");
                         array_push($tableValue,$previousMessageStatus);
                         $stringType .=  "s";
                    } 
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $messageStatusInUser = updateDynamicData($conn,"message"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($messageStatusInUser)
                    {
                         // header('Location: ../viewMessage.php?type=1');
                         if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyThree))
                         {
                              header('Location: ../viewMessage.php?type=1');
                         }
                         else
                         {
                              header('Location: ../viewMessage.php?type=2');
                         }
                    }
                    else
                    {
                         header('Location: ../viewMessage.php?type=2');
                    }
               }
               else
               {
               header('Location: ../viewMessage.php?type=3');
               }

          }
          else
          {
               header('Location: ../viewMessage.php?type=2');
          }
     }
     else
     {
     header('Location: ../viewMessage.php?type=3');
     }
}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyThree)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","user_status","admin_status","reply_one","reply_three"),
     array($uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyThree),"sssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

?>