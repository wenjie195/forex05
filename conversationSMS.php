<?php

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

            $conn = connDB();
            // $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");

            $userSMSDetails = getMessage($conn," WHERE uid = ? AND reply_three = 'NEW' ORDER BY date_created ASC",array("uid"),array($uid),"s");

            if($userSMSDetails)
            {   
                for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                {
                    if($userSMSDetails[$cnt]->getReplyTwo() == 'YES')
                    { 
                    ?>
                        <div class="user-chat-img">
                        	<a href="./uploads/<?php echo $userSMSDetails[$cnt]->getReceiveSMS();?>" data-fancybox >
                				<img src="uploads/<?php echo $userSMSDetails[$cnt]->getReceiveSMS();?>" class="chat-img">
                        	</a>
                        </div>
                        <div class="admin-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReplySMS();?></div>
                    <?php
                    }
                    else
                    {
                    ?>
                        <div class="user-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReceiveSMS();?></div>                
                        <div class="admin-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReplySMS();?></div>
                    <?php
                    }
                    ?>

                <?php
                }
                ?>
            <?php
            }
            else
            {
            }
            $conn->close();
            ?>