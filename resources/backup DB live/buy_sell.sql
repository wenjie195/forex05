-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 10, 2020 at 05:43 PM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dxforext_forex_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buy_sell`
--

INSERT INTO `buy_sell` (`id`, `trade_uid`, `uid`, `username`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `date_created`, `date_updated`) VALUES
(1, 'af0488b5101d3efe9926e44e3bccb952', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 2200, 'GBP/USD', 'SELL', '1.3051', '', 60, '', '2020-03-10 07:46:47', '2020-03-10 09:28:44'),
(2, 'a8af1bfdc0a0b36d985a1b50c34ff975', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 2320, 'GBP/USD', 'SELL', '1.3054', '', 60, '', '2020-03-10 07:48:53', '2020-03-10 09:28:47'),
(3, '882bef69d24249d58f50e006c9d9ec5e', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 2800, 'GBP/USD', 'SELL', '1.3054', '', 60, '', '2020-03-10 07:48:53', '2020-03-10 09:28:49'),
(4, 'eabce97f4495a7f8e65b5ecdc027aaf5', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 1500, 'GBP/USD', 'SELL', '1.3049', '', 60, '', '2020-03-10 07:45:02', '2020-03-10 09:28:52'),
(5, '6b5eea4168daa601d478ce3448114e3a', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 800, 'GBP/USD', 'SELL', '1.3035', '', 60, '', '2020-03-10 07:36:37', '2020-03-10 09:28:55'),
(6, '2c5e04bbb6a6edb8e5ad809f0fa78ad6', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 3500, 'GBP/USD', 'SELL', '1.3053', '', 60, '', '2020-03-10 07:50:00', '2020-03-10 09:28:57'),
(7, 'de4ec1987d8a1da51083e4741ae1dbfe', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 4100, 'GBP/USD', 'SELL', '1.3056', '', 60, '', '2020-03-10 07:54:52', '2020-03-10 09:29:00'),
(8, 'bd990f45d98c4bd48e0e69544bc5ee9c', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 5500, 'GBP/USD', 'SELL', '1.3056', '', 60, '', '2020-03-10 07:51:54', '2020-03-10 09:29:02'),
(9, 'e074e4926d5e138bc967414e4c2fe5bb', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 6800, 'GBP/USD', 'SELL', '1.3053', '', 60, '', '2020-03-10 07:51:54', '2020-03-10 09:29:04'),
(10, '8c12b01d750d5af422a881fae38a5d2a', 'ca6a4bf533eae83823d60b8b9c877b90', NULL, 1200, 'GBP/USD', 'SELL', '1.3053', '', 60, '', '2020-03-10 07:43:53', '2020-03-10 09:29:08'),
(11, 'b6b31c2c0ebec194e5ef6ce2cc852221', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 2200, 'GBP/USD', 'BUY', '1.3052', '1.3049', 60, 'LOSE', '2020-03-10 07:46:47', '2020-03-10 09:29:13'),
(12, '801d4f3d90b30cdcf046fb50c58a0098', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 2320, 'GBP/USD', 'BUY', '1.3054', '1.3055', 60, 'WIN', '2020-03-10 07:48:53', '2020-03-10 09:29:15'),
(13, 'aa13d17239441db67d37e42915c42944', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 2800, 'GBP/USD', 'BUY', '1.3054', '1.3054', 60, 'WIN', '2020-03-10 07:48:53', '2020-03-10 09:29:18'),
(14, 'c8a94163b84ddfa383938daeace5a1b9', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 1200, 'GBP/USD', 'SELL', '1.3034', '', 60, '', '2020-03-10 07:38:14', '2020-03-10 09:29:20'),
(15, 'd7fcc1787be536857d694a15d5a64bb5', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 2000, 'GBP/USD', 'BUY', '1.3041', '1.3048', 60, 'WIN', '2020-03-10 07:41:05', '2020-03-10 09:29:22'),
(16, '2c2805adbd30115dd4fab1a4fbfc5184', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 1500, 'GBP/USD', 'BUY', '1.3048', '1.3048', 60, 'WIN', '2020-03-10 07:45:02', '2020-03-10 09:29:25'),
(17, '4856ed62a9d1c7f136b3872c0d00f570', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 800, 'GBP/USD', 'BUY', '1.3035', '1.3034', 60, 'LOSE', '2020-03-10 07:36:37', '2020-03-10 09:29:27'),
(18, '4a258674f79431358da2083f52e26994', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 3500, 'GBP/USD', 'BUY', '1.3053', '1.3056', 60, 'WIN', '2020-03-10 07:50:00', '2020-03-10 09:29:36'),
(19, '38dff2f1dacd5c815e3b5d2c1875fa61', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 4100, 'GBP/USD', 'BUY', '1.3056', '1.3056', 60, 'WIN', '2020-03-10 07:54:52', '2020-03-10 09:29:39'),
(20, '4347a64102fdb7a1af9537621678dc34', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 5500, 'GBP/USD', 'BUY', '1.3056', '1.3056', 60, 'WIN', '2020-03-10 07:51:54', '2020-03-10 09:29:42'),
(21, '6dd5089d306a3022341a8234e47a852f', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 6800, 'GBP/USD', 'BUY', '1.3052', '1.3053', 60, 'WIN', '2020-03-10 07:51:54', '2020-03-10 09:29:44'),
(22, 'bea27354f1274dec26c7c7c8cb624ed9', '5c5fdb4c0ef833d562434e0cdacd952d', NULL, 1200, 'GBP/USD', 'BUY', '1.3052', '1.3047', 60, 'LOSE', '2020-03-10 07:43:53', '2020-03-10 09:29:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trade_uid` (`trade_uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
