-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2020 at 03:25 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_exgame`
--

-- --------------------------------------------------------

--
-- Table structure for table `bet_status`
--

CREATE TABLE `bet_status` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'ORI',
  `status_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_status`
--

INSERT INTO `bet_status` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `result_edited`, `status`, `status_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, 'd5a5dd50a57cd9ec336bef4ac6782606', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10000', 1000, 'EUR/USD', 'SELL', '4.4', '4.4005', 30, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:40:00', '2020-02-21 01:40:00'),
(2, '2ba9823fa17f48695970a1eb6b37cb7f', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9000', 2000, 'EUR/USD', 'BUY', '4.4', '4.4005', 30, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-21 01:40:24', '2020-02-21 01:40:24'),
(3, 'f9567534857f1ff35462192d27b95f51', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '11000', 100, 'EUR/USD', 'SELL', '4.4', '4.4005', 30, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:41:00', '2020-02-21 01:41:00'),
(4, 'a2b9072e8cc99d07b9dfc5176a782e93', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10900', 300, 'EUR/USD', 'SELL', '4.4', '4.4005', 30, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:41:31', '2020-02-21 01:41:31'),
(5, '515411c2aacca08f2689e86fc24fbe3c', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10600', 1000, 'EUR/USD', 'BUY', '4.4', '4.3995', 60, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:42:17', '2020-02-21 01:42:17'),
(6, 'f0fae985ea9d2f67bcaefe3feb189984', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9600', 2000, 'USD/EUR', 'SELL', '4.4', '4.4005', 60, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:42:34', '2020-02-21 01:42:34'),
(7, '4f04ddc2ff9cb7b89fb4a6afda689c49', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7600', 100, 'USD/EUR', 'BUY', '4.4', '4.3995', 60, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:42:49', '2020-02-21 01:42:49'),
(8, '3b9dbe10c3b1029cda4bc46e8e15dd7b', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7500', 300, 'USD/EUR', 'SELL', '4.4', '4.3995', 60, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-21 01:43:09', '2020-02-21 01:43:09'),
(9, 'fa0e0718d9186fc79dd49b14e8e3afbb', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7800', 500, 'EUR/USD', 'BUY', '4.4', '4.4005', 180, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-21 01:43:37', '2020-02-21 01:43:37'),
(10, '165126e44c896685a48f1a724772b721', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '8300', 1000, 'EUR/USD', 'BUY', '4.4', '4.3995', 180, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:44:00', '2020-02-21 01:44:00'),
(11, 'd24c43df6650b6d6ddcf845576e32c7c', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7300', 2000, 'USD/EUR', 'BUY', '4.4', '4.4005', 180, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-21 01:44:31', '2020-02-21 01:44:31'),
(12, '6dac6b44071b841a90b6708cbff32026', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9300', 5000, 'GBP/JPY', 'SELL', '4.4', '4.3995', 180, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-21 01:44:52', '2020-02-21 01:44:52'),
(13, 'fc2d2a595bb8818cf8102ced46c89dba', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '14300', 100, 'USD/EUR', 'SELL', '4.4', '4.4', 180, 'DRAW', 'DRAW', 'ORI', NULL, NULL, '2020-02-21 01:46:07', '2020-02-21 01:46:07'),
(14, '8223ecdde552dc184c4760282e9e7aa7', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '14300', 500, 'GBP/JPY', 'SELL', '4.4', '4.4005', 180, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:46:46', '2020-02-21 01:46:46'),
(15, 'b58644dd4e35e1c8d5b75dde54543e91', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '13800', 1000, 'USD/CAD', 'BUY', '4.4', '4.3995', 180, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:47:43', '2020-02-21 01:47:43'),
(16, '38fd52593b3e87e2cf7ea598897d81e0', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '12800', 2000, 'USD/EUR', 'BUY', '4.4', '4.3995', 180, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-21 01:48:03', '2020-02-21 01:48:03'),
(17, 'fb59438b09bb9e6643ef4aeb88cdbb5a', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10800', 5000, 'USD/AUD', 'BUY', '4.4', '4.4005', 180, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-21 01:48:33', '2020-02-21 01:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `submit_date` varchar(255) DEFAULT NULL,
  `submit_time` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `verify_by` varchar(255) DEFAULT NULL,
  `verify_time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `edit_record`
--

CREATE TABLE `edit_record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record`
--

INSERT INTO `record` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `date_created`, `date_updated`) VALUES
(1, 'd5a5dd50a57cd9ec336bef4ac6782606', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10000', 1000, 'EUR/USD', 'SELL', '4.4', '4.4005', 30, 'LOSE', '2020-02-21 01:40:00', '2020-02-21 01:40:00'),
(2, '2ba9823fa17f48695970a1eb6b37cb7f', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9000', 2000, 'EUR/USD', 'BUY', '4.4', '4.4005', 30, 'WIN', '2020-02-21 01:40:24', '2020-02-21 01:40:24'),
(3, 'f9567534857f1ff35462192d27b95f51', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '11000', 100, 'EUR/USD', 'SELL', '4.4', '4.4005', 30, 'LOSE', '2020-02-21 01:41:00', '2020-02-21 01:41:00'),
(4, 'a2b9072e8cc99d07b9dfc5176a782e93', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10900', 300, 'EUR/USD', 'SELL', '4.4', '4.4005', 30, 'LOSE', '2020-02-21 01:41:31', '2020-02-21 01:41:31'),
(5, '515411c2aacca08f2689e86fc24fbe3c', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10600', 1000, 'EUR/USD', 'BUY', '4.4', '4.3995', 60, 'LOSE', '2020-02-21 01:42:17', '2020-02-21 01:42:17'),
(6, 'f0fae985ea9d2f67bcaefe3feb189984', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9600', 2000, 'USD/EUR', 'SELL', '4.4', '4.4005', 60, 'LOSE', '2020-02-21 01:42:34', '2020-02-21 01:42:34'),
(7, '4f04ddc2ff9cb7b89fb4a6afda689c49', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7600', 100, 'USD/EUR', 'BUY', '4.4', '4.3995', 60, 'LOSE', '2020-02-21 01:42:49', '2020-02-21 01:42:49'),
(8, '3b9dbe10c3b1029cda4bc46e8e15dd7b', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7500', 300, 'USD/EUR', 'SELL', '4.4', '4.3995', 60, 'WIN', '2020-02-21 01:43:09', '2020-02-21 01:43:09'),
(9, 'fa0e0718d9186fc79dd49b14e8e3afbb', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7800', 500, 'EUR/USD', 'BUY', '4.4', '4.4005', 180, 'WIN', '2020-02-21 01:43:37', '2020-02-21 01:43:37'),
(10, '165126e44c896685a48f1a724772b721', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '8300', 1000, 'EUR/USD', 'BUY', '4.4', '4.3995', 180, 'LOSE', '2020-02-21 01:44:00', '2020-02-21 01:44:00'),
(11, 'd24c43df6650b6d6ddcf845576e32c7c', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '7300', 2000, 'USD/EUR', 'BUY', '4.4', '4.4005', 180, 'WIN', '2020-02-21 01:44:31', '2020-02-21 01:44:31'),
(12, '6dac6b44071b841a90b6708cbff32026', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9300', 5000, 'GBP/JPY', 'SELL', '4.4', '4.3995', 180, 'WIN', '2020-02-21 01:44:52', '2020-02-21 01:44:52'),
(13, 'fc2d2a595bb8818cf8102ced46c89dba', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '14300', 100, 'USD/EUR', 'SELL', '4.4', '4.4', 180, 'DRAW', '2020-02-21 01:46:07', '2020-02-21 01:46:07'),
(14, '8223ecdde552dc184c4760282e9e7aa7', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '14300', 500, 'GBP/JPY', 'SELL', '4.4', '4.4005', 180, 'LOSE', '2020-02-21 01:46:46', '2020-02-21 01:46:46'),
(15, 'b58644dd4e35e1c8d5b75dde54543e91', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '13800', 1000, 'USD/CAD', 'BUY', '4.4', '4.3995', 180, 'LOSE', '2020-02-21 01:47:43', '2020-02-21 01:47:43'),
(16, '38fd52593b3e87e2cf7ea598897d81e0', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '12800', 2000, 'USD/EUR', 'BUY', '4.4', '4.3995', 180, 'LOSE', '2020-02-21 01:48:03', '2020-02-21 01:48:03'),
(17, 'fb59438b09bb9e6643ef4aeb88cdbb5a', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10800', 5000, 'USD/AUD', 'BUY', '4.4', '4.4005', 180, 'WIN', '2020-02-21 01:48:33', '2020-02-21 01:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'upline uid',
  `referral_id` varchar(255) NOT NULL COMMENT 'new user uid',
  `referral_name` varchar(255) NOT NULL COMMENT 'new user name',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `deposit` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL COMMENT 'win or lose',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `deposit`, `credit`, `bank_name`, `bank_account_no`, `bank_account_name`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '6fd68af94270ba1521e05e94c28c7914', 'admin', 'admin@gmail.com', 'd1794d9b452ae10d1b135c1c05da98b0b2170ef2c1280b8d254239b8a420435a', 'fe04ef8fc9368dcbe8be35855763d4c24641f5d5', '0123456', 'ADMIN admin', 'admin, address', NULL, '0', '0', NULL, NULL, NULL, 1, 0, '2020-02-05 04:01:57', '2020-02-10 05:06:10'),
(2, 'ad5b56f368d7456e60abf9d7852f7088', 'user1', 'oliver@gmail.com', '3942263cb2c6927524fa60b43a3870ecd3390e6d4e750dc6f3eb933bcb830dbe', 'd02a770dacf417b3eb860d19b5f129e7ba28c4ae', '963+999', 'Oliver Queen', 'user1, address.', NULL, '50000', '15800', NULL, NULL, NULL, 1, 1, '2020-02-06 04:51:31', '2020-02-21 01:48:33'),
(8, '56b23a71fa4244ede9d479c3345907f0', 'user2', 'user2@gg.cc', 'd3ac44dd2ae23bce4d4a839adbb844e810b9dd9794cd193b39c0e79374689515', '2e29b5ca67d2ccc0877ad11148dd157bc7df090a', '+123-445566', NULL, NULL, NULL, '5000', '10000', NULL, NULL, NULL, 1, 1, '2020-02-14 03:32:51', '2020-02-20 09:43:21');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL COMMENT 'withdrawal amount',
  `current_credit` varchar(255) DEFAULT NULL COMMENT 'current credit in account',
  `charges` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_datetime` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bet_status`
--
ALTER TABLE `bet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edit_record`
--
ALTER TABLE `edit_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bet_status`
--
ALTER TABLE `bet_status`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `edit_record`
--
ALTER TABLE `edit_record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
