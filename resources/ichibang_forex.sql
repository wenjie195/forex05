-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 02, 2020 at 03:08 PM
-- Server version: 5.6.47-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ichibang_forex`
--

-- --------------------------------------------------------

--
-- Table structure for table `bet_status`
--

CREATE TABLE `bet_status` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'ORI',
  `status_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_status`
--

INSERT INTO `bet_status` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `result_edited`, `status`, `status_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, '852374a790fbf5cd5282ae9f2ae8ffe6', '9cee75f630eb3d84217787664a47a4e6', 'dragon', '9900', 100, 'GBP/JPY', 'SELL', '139.0910', '', 30, '', '', 'ORI', NULL, NULL, '2020-03-02 07:02:52', '2020-03-02 07:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buy_sell`
--

INSERT INTO `buy_sell` (`id`, `trade_uid`, `uid`, `username`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `date_created`, `date_updated`) VALUES
(1, '852374a790fbf5cd5282ae9f2ae8ffe6', '9cee75f630eb3d84217787664a47a4e6', NULL, 100, 'GBP/JPY', 'SELL', '139.0910', '', 30, '', '2020-03-02 07:03:24', '2020-03-02 07:03:24');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL,
  `ch_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `submit_date` varchar(255) DEFAULT NULL,
  `submit_time` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `verify_by` varchar(255) DEFAULT NULL,
  `verify_time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deposit`
--

INSERT INTO `deposit` (`id`, `uid`, `username`, `bank_name`, `amount`, `submit_date`, `submit_time`, `status`, `reference`, `verify_by`, `verify_time`, `date_created`, `date_updated`) VALUES
(1, 'f0e387766527547188dc45c20fd9f661', 'dragon', NULL, '10000', NULL, NULL, NULL, NULL, 'admin', '2020-03-02 15:02:20', '2020-03-02 07:02:20', '2020-03-02 07:02:20'),
(2, '5a73ad68bf04347b7c2ecbc05bc46640', 'user1', NULL, '10000', NULL, NULL, NULL, NULL, 'admin', '2020-03-02 15:03:52', '2020-03-02 07:03:52', '2020-03-02 07:03:52'),
(3, '31c8537eec63ee9a74dba93dfb40fdfd', 'mike', NULL, '10000', NULL, NULL, NULL, NULL, 'admin', '2020-03-02 15:07:35', '2020-03-02 07:07:35', '2020-03-02 07:07:35');

-- --------------------------------------------------------

--
-- Table structure for table `edit_record`
--

CREATE TABLE `edit_record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `reply_one` varchar(255) DEFAULT NULL,
  `reply_two` varchar(255) DEFAULT NULL,
  `reply_three` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `uid`, `message_uid`, `receive_message`, `reply_message`, `reply_one`, `reply_two`, `reply_three`, `user_status`, `admin_status`, `date_created`, `date_updated`) VALUES
(1, '8a8b12cd22e2dea4d13b61bf73e457fe', '031912efc5ce188cac2bd14c60f2cf8a', '(系统讯息) 要求充值，请联系我!', 'okok', '031912efc5ce188cac2bd14c60f2cf8a', NULL, NULL, 'GET', 'REPLY', '2020-03-02 06:59:47', '2020-03-02 07:03:39'),
(2, '9cee75f630eb3d84217787664a47a4e6', '3c0e98462793797df3bae4b6fa19e189', '(系统讯息) 要求充值，请联系我!', NULL, '3c0e98462793797df3bae4b6fa19e189', NULL, NULL, 'SENT', 'GET', '2020-03-02 07:01:02', '2020-03-02 07:01:02'),
(3, '9cee75f630eb3d84217787664a47a4e6', '1768fabbf6bc2df9f111322df3c789c2', '(系统讯息) 要求充值，请联系我!', NULL, '1768fabbf6bc2df9f111322df3c789c2', NULL, NULL, 'SENT', 'GET', '2020-03-02 07:01:12', '2020-03-02 07:01:12'),
(4, '9cee75f630eb3d84217787664a47a4e6', '3d4da189d38a286a3b0a3c73ad50aa7f', '(系统讯息) 要求充值，请联系我!', NULL, '3d4da189d38a286a3b0a3c73ad50aa7f', NULL, NULL, 'SENT', 'GET', '2020-03-02 07:01:12', '2020-03-02 07:01:12'),
(5, '9cee75f630eb3d84217787664a47a4e6', 'ea3a9223399f1d79a320e6543a3e8948', '(系统讯息) 要求充值，请联系我!', 'done', 'ea3a9223399f1d79a320e6543a3e8948', NULL, NULL, 'GET', 'REPLY', '2020-03-02 07:01:35', '2020-03-02 07:01:54');

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record`
--

INSERT INTO `record` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `date_created`, `date_updated`) VALUES
(1, '852374a790fbf5cd5282ae9f2ae8ffe6', '9cee75f630eb3d84217787664a47a4e6', 'dragon', '9900', 100, 'GBP/JPY', 'SELL', '139.0910', '', 30, '', '2020-03-02 07:02:52', '2020-03-02 07:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'upline uid',
  `referral_id` varchar(255) NOT NULL COMMENT 'new user uid',
  `referral_name` varchar(255) NOT NULL COMMENT 'new user name',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `deposit` varchar(255) DEFAULT '0',
  `credit` varchar(255) DEFAULT '0' COMMENT 'win or lose',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `deposit`, `credit`, `bank_name`, `bank_account_no`, `bank_account_name`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '6fd68af94270ba1521e05e94c28c7914', 'admin', 'admin@gmail.com', 'd1794d9b452ae10d1b135c1c05da98b0b2170ef2c1280b8d254239b8a420435a', 'fe04ef8fc9368dcbe8be35855763d4c24641f5d5', '0123456', 'ADMIN admin', 'admin, address', NULL, '100', '-300', NULL, NULL, NULL, 1, 0, '2020-02-05 04:01:57', '2020-02-28 01:38:26'),
(12, 'ee47a63b6ce9dedf08906f21d2d2a88d', 'yahoo', 'likkit1990@yahoo.com', '533c05bd94c96f0dc9191958439f5f41db56f3de9bf67b89b35c338c292748e0', '8b985ce8ea6c68a5e02810d01c1e0682d9269996', '11', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, 1, 1, '2020-03-02 04:42:11', '2020-03-02 04:42:11'),
(13, 'daaf32ee346d17df63030886280e28e0', 'user1', 'wenjie195.vidatech@gmail.com', 'cbbdccaaddc08e8c71902ce5fe47c34d1c4a105d867dd32fbd4ab4afb731d84d', 'b20b58638525e7d38f4219471c8af41e9a6c689a', '+74196385--', NULL, NULL, NULL, '10000', '10000', NULL, NULL, NULL, 1, 1, '2020-03-02 04:46:44', '2020-03-02 07:03:52'),
(14, '8355c08d0139faa50c6d2ee599db722b', 'super', 'michaelwong.vidatech@gmail.com', '6071ec5b5f4662987b40fcffd9350795038816d982f6ab80f0427d12e376bdf7', '50b400af4922aca819c45f46e7e966a7df00d183', '01022223333', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, 1, 1, '2020-03-02 06:55:17', '2020-03-02 06:55:17'),
(15, '8a8b12cd22e2dea4d13b61bf73e457fe', 'mike', 'likkit1990@gmail.com', '0024760a019a3f43154611d47b174f3f0d06f85bad012d452df82c09447e8e12', '35d566b6e74bd8e442b8fb6dd8e1ff130b20dc74', '123', NULL, NULL, NULL, '10000', '10000', NULL, NULL, NULL, 1, 1, '2020-03-02 06:56:07', '2020-03-02 07:07:35'),
(16, '9cee75f630eb3d84217787664a47a4e6', 'dragon', 'dragon86@gmail.com', 'ddb0f80285f5e0e1221f453d29d30596f1d89aeea0a76dcaef9cc90320da3168', 'c5c37bf803a5ac02ed0c767cc3d214fd394325f2', '01188695461', NULL, NULL, NULL, '10000', '9900', NULL, NULL, NULL, 1, 1, '2020-03-02 07:00:55', '2020-03-02 07:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL COMMENT 'withdrawal amount',
  `current_credit` varchar(255) DEFAULT NULL COMMENT 'current credit in account',
  `charges` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_datetime` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bet_status`
--
ALTER TABLE `bet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trade_uid` (`trade_uid`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edit_record`
--
ALTER TABLE `edit_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bet_status`
--
ALTER TABLE `bet_status`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `edit_record`
--
ALTER TABLE `edit_record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
