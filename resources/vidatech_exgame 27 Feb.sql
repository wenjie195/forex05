-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2020 at 11:03 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_exgame`
--

-- --------------------------------------------------------

--
-- Table structure for table `bet_status`
--

CREATE TABLE `bet_status` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'ORI',
  `status_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_status`
--

INSERT INTO `bet_status` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `result_edited`, `status`, `status_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, '6cd9ef345a57e27cd377bcae0c546d24', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9375', 1000, 'EUR/USD', 'SELL', '1.0866', '1.0867', 30, 'LOSE', 'LOSE', 'ORI', NULL, NULL, '2020-02-26 04:40:59', '2020-02-26 04:40:59'),
(2, '33a3f867b7a5c781364d03ed12228afa', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '8375', 2000, 'EUR/USD', 'SELL', '1.0867', '1.0864', 30, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-26 04:41:51', '2020-02-26 04:41:51'),
(3, 'f54c746d57e1b3159bfb33795bcb39b4', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10375', 2000, 'AUD/USD', 'SELL', '0.6594', '0.6591', 30, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-26 04:43:22', '2020-02-26 04:43:22'),
(4, '45ef9fa0eec6fa98ab4d0949fe41f847', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '12375', 1000, 'EUR/USD', 'SELL', '1.0868', '1.0865', 30, 'WIN', 'WIN', 'ORI', NULL, NULL, '2020-02-26 04:59:33', '2020-02-26 04:59:33'),
(5, '345b1fe75b594450c84e66c69610d6ef', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '13375', 2000, 'EUR/USD', 'BUY', '1.0867', '1.0874', 30, 'WIN', 'LOSE', 'EDITED', NULL, 'admin', '2020-02-26 04:59:44', '2020-02-26 05:00:11'),
(6, '7d39c221bb2c70aa0c9ac915845b4055', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '15375', 3000, 'EUR/USD', 'SELL', '1.0868', '1.0869', 30, 'LOSE', 'WIN', 'EDITED', NULL, 'admin', '2020-02-26 04:59:55', '2020-02-26 05:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `submit_date` varchar(255) DEFAULT NULL,
  `submit_time` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `verify_by` varchar(255) DEFAULT NULL,
  `verify_time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deposit`
--

INSERT INTO `deposit` (`id`, `uid`, `username`, `bank_name`, `amount`, `submit_date`, `submit_time`, `status`, `reference`, `verify_by`, `verify_time`, `date_created`, `date_updated`) VALUES
(1, '8b0fce1fae3b3de92254640f456e2b91', 'user1', NULL, '5000', NULL, NULL, NULL, NULL, 'admin', '2020-02-26 12:30:20', '2020-02-26 04:30:20', '2020-02-26 04:30:20'),
(2, '64460a942ff5d58ca3260b552578da6c', 'user1', NULL, '2500', NULL, NULL, NULL, NULL, 'admin', '2020-02-26 12:30:27', '2020-02-26 04:30:27', '2020-02-26 04:30:27'),
(3, '69faff2fd51003d584808010bd958d8f', 'user1', NULL, '1250', NULL, NULL, NULL, NULL, 'admin', '2020-02-26 12:30:41', '2020-02-26 04:30:41', '2020-02-26 04:30:41'),
(4, 'f0a77593abf6af7f755d58592cde4c4e', 'user1', NULL, '625', NULL, NULL, NULL, NULL, 'admin', '2020-02-26 12:31:13', '2020-02-26 04:31:13', '2020-02-26 04:31:13');

-- --------------------------------------------------------

--
-- Table structure for table `edit_record`
--

CREATE TABLE `edit_record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `edit_record`
--

INSERT INTO `edit_record` (`id`, `trade_uid`, `uid`, `amount`, `result`, `result_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, '345b1fe75b594450c84e66c69610d6ef', 'ad5b56f368d7456e60abf9d7852f7088', 2000, 'WIN', 'LOSE', 'admin', '2020-02-26 05:00:11', '2020-02-26 05:00:11'),
(2, '7d39c221bb2c70aa0c9ac915845b4055', 'ad5b56f368d7456e60abf9d7852f7088', 3000, 'LOSE', 'WIN', 'admin', '2020-02-26 05:00:21', '2020-02-26 05:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `reply_one` varchar(255) DEFAULT NULL,
  `reply_two` varchar(255) DEFAULT NULL,
  `reply_three` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `uid`, `message_uid`, `receive_message`, `reply_message`, `reply_one`, `reply_two`, `reply_three`, `user_status`, `admin_status`, `date_created`, `date_updated`) VALUES
(1, 'ad5b56f368d7456e60abf9d7852f7088', '14ad37e598a988fc0f79d1bffea2cfc6', 'hi', 'hello', 'ahahaha', NULL, NULL, ' ', 'GET', '2020-02-27 09:43:54', '2020-02-27 09:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record`
--

INSERT INTO `record` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `date_created`, `date_updated`) VALUES
(1, '6cd9ef345a57e27cd377bcae0c546d24', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '9375', 1000, 'EUR/USD', 'SELL', '1.0866', '1.0867', 30, 'LOSE', '2020-02-26 04:40:59', '2020-02-26 04:40:59'),
(2, '33a3f867b7a5c781364d03ed12228afa', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '8375', 2000, 'EUR/USD', 'SELL', '1.0867', '1.0864', 30, 'WIN', '2020-02-26 04:41:51', '2020-02-26 04:41:51'),
(3, 'f54c746d57e1b3159bfb33795bcb39b4', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '10375', 2000, 'AUD/USD', 'SELL', '0.6594', '0.6591', 30, 'WIN', '2020-02-26 04:43:22', '2020-02-26 04:43:22'),
(4, '45ef9fa0eec6fa98ab4d0949fe41f847', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '12375', 1000, 'EUR/USD', 'SELL', '1.0868', '1.0865', 30, 'WIN', '2020-02-26 04:59:33', '2020-02-26 04:59:33'),
(5, '345b1fe75b594450c84e66c69610d6ef', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '13375', 2000, 'EUR/USD', 'BUY', '1.0867', '1.0874', 30, 'WIN', '2020-02-26 04:59:44', '2020-02-26 04:59:44'),
(6, '7d39c221bb2c70aa0c9ac915845b4055', 'ad5b56f368d7456e60abf9d7852f7088', 'user1', '15375', 3000, 'EUR/USD', 'SELL', '1.0868', '1.0869', 30, 'LOSE', '2020-02-26 04:59:55', '2020-02-26 04:59:55');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'upline uid',
  `referral_id` varchar(255) NOT NULL COMMENT 'new user uid',
  `referral_name` varchar(255) NOT NULL COMMENT 'new user name',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `deposit` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL COMMENT 'win or lose',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `deposit`, `credit`, `bank_name`, `bank_account_no`, `bank_account_name`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '6fd68af94270ba1521e05e94c28c7914', 'admin', 'admin@gmail.com', 'd1794d9b452ae10d1b135c1c05da98b0b2170ef2c1280b8d254239b8a420435a', 'fe04ef8fc9368dcbe8be35855763d4c24641f5d5', '0123456', 'ADMIN admin', 'admin, address', NULL, '0', '0', NULL, NULL, NULL, 1, 0, '2020-02-05 04:01:57', '2020-02-10 05:06:10'),
(2, 'ad5b56f368d7456e60abf9d7852f7088', 'user1', 'oliver@gmail.com', '3942263cb2c6927524fa60b43a3870ecd3390e6d4e750dc6f3eb933bcb830dbe', 'd02a770dacf417b3eb860d19b5f129e7ba28c4ae', '963+999', 'Barry Allen', 'user1, address.', NULL, '9375', '3375', NULL, '123-123+456+456', NULL, 1, 1, '2020-02-06 04:51:31', '2020-02-27 08:32:04'),
(8, '56b23a71fa4244ede9d479c3345907f0', 'user2', 'user2@gg.cc', 'd3ac44dd2ae23bce4d4a839adbb844e810b9dd9794cd193b39c0e79374689515', '2e29b5ca67d2ccc0877ad11148dd157bc7df090a', '+123-445566', NULL, NULL, NULL, '0', '0', NULL, '456-456-852-852', NULL, 1, 1, '2020-02-14 03:32:51', '2020-02-26 04:22:09');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL COMMENT 'withdrawal amount',
  `current_credit` varchar(255) DEFAULT NULL COMMENT 'current credit in account',
  `charges` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_datetime` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`id`, `uid`, `username`, `contact`, `bank_name`, `bank_acc_number`, `amount`, `current_credit`, `charges`, `status`, `reference`, `approved_by`, `approved_datetime`, `date_created`, `date_updated`) VALUES
(1, 'ad5b56f368d7456e60abf9d7852f7088', 'user 1', NULL, 'CIMB', '+741-852*963/', '10000', '13375', NULL, 'PENDING', NULL, NULL, NULL, '2020-02-27 08:30:45', '2020-02-27 08:30:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bet_status`
--
ALTER TABLE `bet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edit_record`
--
ALTER TABLE `edit_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bet_status`
--
ALTER TABLE `bet_status`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `edit_record`
--
ALTER TABLE `edit_record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
