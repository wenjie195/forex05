-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2020 at 05:52 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_exgame`
--

-- --------------------------------------------------------

--
-- Table structure for table `bet_status`
--

CREATE TABLE `bet_status` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_status`
--

INSERT INTO `bet_status` (`id`, `trade_uid`, `uid`, `username`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `result_edited`, `status`, `status_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, 'e4f11b6380aeedab59c0bcafb8414f95', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 100, 'GBP', 'Buy', '0.831624', '0.832124', 60, 'WIN', 'WIN', NULL, NULL, 'admin', '2020-02-17 09:00:50', '2020-02-19 04:44:54'),
(2, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 250, 'GBP', 'Buy', '0.83128', '0.83178', 30, 'WIN', 'LOSE', NULL, NULL, 'admin', '2020-02-17 09:07:12', '2020-02-19 03:30:12'),
(3, 'c60c9a0139ec486853871269b66cdeb0', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 1000, 'GBP', 'Buy', '0.83128', '0.83178', 60, 'WIN', 'WIN', NULL, NULL, 'admin', '2020-02-17 09:08:34', '2020-02-19 03:34:07'),
(4, 'c8e3f0bfb0b9ebcab54760746ba46408', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 500, 'GBP', 'Buy', '0.83128', '0.83178', 30, 'WIN', 'WIN', NULL, NULL, 'admin', '2020-02-17 09:08:52', '2020-02-19 03:34:09'),
(5, '9027a932b4dcbdcd8ce85d09d87dd248', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 1000, 'GBP', 'Buy', '0.833204', '0.833704', 60, 'WIN', 'LOSE', NULL, NULL, 'admin', '2020-02-18 01:28:15', '2020-02-19 03:34:10'),
(6, '67c9870dedad40c0c36c4f9e3054e55b', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 100, 'EUR/USD', 'BUY', '4.4', '4.4005', 30, NULL, NULL, NULL, NULL, NULL, '2020-02-19 02:30:49', '2020-02-19 02:30:49');

-- --------------------------------------------------------

--
-- Table structure for table `buy_sell`
--

CREATE TABLE `buy_sell` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `submit_date` varchar(255) DEFAULT NULL,
  `submit_time` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `verify_by` varchar(255) DEFAULT NULL,
  `verify_time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `edit_record`
--

CREATE TABLE `edit_record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `edit_record`
--

INSERT INTO `edit_record` (`id`, `trade_uid`, `uid`, `amount`, `result`, `result_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, 'e4f11b6380aeedab59c0bcafb8414f95', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-19 02:58:33', '2020-02-19 02:58:33'),
(2, 'e4f11b6380aeedab59c0bcafb8414f95', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'WIN', 'admin', '2020-02-19 02:58:54', '2020-02-19 02:58:54'),
(3, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'LOSE', 'admin', '2020-02-19 03:02:33', '2020-02-19 03:02:33'),
(4, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'WIN', 'admin', '2020-02-19 03:02:38', '2020-02-19 03:02:38'),
(5, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'LOSE', 'admin', '2020-02-19 03:05:46', '2020-02-19 03:05:46'),
(6, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'WIN', 'admin', '2020-02-19 03:05:47', '2020-02-19 03:05:47'),
(7, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'LOSE', 'admin', '2020-02-19 03:05:48', '2020-02-19 03:05:48'),
(8, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'WIN', 'admin', '2020-02-19 03:05:49', '2020-02-19 03:05:49'),
(9, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'LOSE', 'admin', '2020-02-19 03:30:11', '2020-02-19 03:30:11'),
(10, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'WIN', 'admin', '2020-02-19 03:30:11', '2020-02-19 03:30:11'),
(11, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', 250, 'WIN', 'LOSE', 'admin', '2020-02-19 03:30:12', '2020-02-19 03:30:12'),
(12, 'c60c9a0139ec486853871269b66cdeb0', 'ad5b56f368d7456e60abf9d7852f7088', 1000, 'WIN', 'LOSE', 'admin', '2020-02-19 03:34:06', '2020-02-19 03:34:06'),
(13, 'c60c9a0139ec486853871269b66cdeb0', 'ad5b56f368d7456e60abf9d7852f7088', 1000, 'WIN', 'WIN', 'admin', '2020-02-19 03:34:07', '2020-02-19 03:34:07'),
(14, 'c8e3f0bfb0b9ebcab54760746ba46408', 'ad5b56f368d7456e60abf9d7852f7088', 500, 'WIN', 'LOSE', 'admin', '2020-02-19 03:34:08', '2020-02-19 03:34:08'),
(15, 'c8e3f0bfb0b9ebcab54760746ba46408', 'ad5b56f368d7456e60abf9d7852f7088', 500, 'WIN', 'WIN', 'admin', '2020-02-19 03:34:09', '2020-02-19 03:34:09'),
(16, '9027a932b4dcbdcd8ce85d09d87dd248', 'ad5b56f368d7456e60abf9d7852f7088', 1000, 'WIN', 'LOSE', 'admin', '2020-02-19 03:34:10', '2020-02-19 03:34:10'),
(17, 'e4f11b6380aeedab59c0bcafb8414f95', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-19 04:44:44', '2020-02-19 04:44:44'),
(18, 'e4f11b6380aeedab59c0bcafb8414f95', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'WIN', 'admin', '2020-02-19 04:44:54', '2020-02-19 04:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `uid`, `message_uid`, `receive_message`, `reply_message`, `user_status`, `admin_status`, `date_created`, `date_updated`) VALUES
(1, 'ad5b56f368d7456e60abf9d7852f7088', '0ab88136664a909ff3fa4c61d68e8c3e', 'hi, how to trade ?', 'asd', 'GET', 'REPLY', '2020-02-18 07:44:05', '2020-02-19 04:11:27'),
(2, 'ad5b56f368d7456e60abf9d7852f7088', '18dc7893d8a818a321b35c98af9a493c', 'aloho hawaiii', NULL, NULL, 'GET', '2020-02-19 02:02:53', '2020-02-19 02:02:53');

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record`
--

INSERT INTO `record` (`id`, `trade_uid`, `uid`, `username`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `date_created`, `date_updated`) VALUES
(1, 'e4f11b6380aeedab59c0bcafb8414f95', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 100, 'GBP', 'Buy', '0.831624', '0.832124', 60, 'WIN', '2020-02-17 09:00:50', '2020-02-17 09:00:50'),
(2, '64a36beb0cb5589bd000c7c8a6574821', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 250, 'GBP', 'Buy', '0.83128', '0.83178', 30, 'WIN', '2020-02-17 09:07:12', '2020-02-17 09:07:12'),
(3, 'c60c9a0139ec486853871269b66cdeb0', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 1000, 'GBP', 'Buy', '0.83128', '0.83178', 60, 'WIN', '2020-02-17 09:08:34', '2020-02-17 09:08:34'),
(4, 'c8e3f0bfb0b9ebcab54760746ba46408', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 500, 'GBP', 'Buy', '0.83128', '0.83178', 30, 'WIN', '2020-02-17 09:08:52', '2020-02-17 09:08:52'),
(5, '9027a932b4dcbdcd8ce85d09d87dd248', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 1000, 'GBP', 'Buy', '0.833204', '0.833704', 60, 'WIN', '2020-02-18 01:28:15', '2020-02-18 01:28:15'),
(6, '67c9870dedad40c0c36c4f9e3054e55b', 'ad5b56f368d7456e60abf9d7852f7088', NULL, 100, 'EUR/USD', 'BUY', '4.4', '4.4005', 30, NULL, '2020-02-19 02:30:49', '2020-02-19 02:30:49');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'upline uid',
  `referral_id` varchar(255) NOT NULL COMMENT 'new user uid',
  `referral_name` varchar(255) NOT NULL COMMENT 'new user name',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `deposit` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL COMMENT 'win or lose',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `deposit`, `credit`, `bank_name`, `bank_account_no`, `bank_account_name`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '6fd68af94270ba1521e05e94c28c7914', 'admin', 'admin@gmail.com', 'd1794d9b452ae10d1b135c1c05da98b0b2170ef2c1280b8d254239b8a420435a', 'fe04ef8fc9368dcbe8be35855763d4c24641f5d5', '0123456', 'ADMIN admin', 'admin, address', NULL, '0', '0', NULL, NULL, NULL, 1, 0, '2020-02-05 04:01:57', '2020-02-10 05:06:10'),
(2, 'ad5b56f368d7456e60abf9d7852f7088', 'oliver', 'oliver@gmail.com', '3942263cb2c6927524fa60b43a3870ecd3390e6d4e750dc6f3eb933bcb830dbe', 'd02a770dacf417b3eb860d19b5f129e7ba28c4ae', '963+999', 'Oliver Queen', 'user1, address.', NULL, '50', '8900', NULL, NULL, NULL, 1, 1, '2020-02-06 04:51:31', '2020-02-19 04:44:54'),
(8, '56b23a71fa4244ede9d479c3345907f0', 'user2', 'user2@gg.cc', 'd3ac44dd2ae23bce4d4a839adbb844e810b9dd9794cd193b39c0e79374689515', '2e29b5ca67d2ccc0877ad11148dd157bc7df090a', '+123-445566', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-02-14 03:32:51', '2020-02-14 03:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL COMMENT 'withdrawal amount',
  `current_credit` varchar(255) DEFAULT NULL COMMENT 'current credit in account',
  `charges` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_datetime` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`id`, `uid`, `username`, `contact`, `bank_name`, `bank_acc_number`, `amount`, `current_credit`, `charges`, `status`, `reference`, `approved_by`, `approved_datetime`, `date_created`, `date_updated`) VALUES
(1, 'ad5b56f368d7456e60abf9d7852f7088', 'user1 acc', NULL, 'cimb', NULL, '100', '10000', NULL, 'PENDING', NULL, NULL, NULL, '2020-02-19 04:07:20', '2020-02-19 04:07:20'),
(2, 'ad5b56f368d7456e60abf9d7852f7088', 'user1 test acc', NULL, 'rhb', NULL, '1000', '9900', NULL, 'PENDING', NULL, NULL, NULL, '2020-02-19 04:10:18', '2020-02-19 04:10:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bet_status`
--
ALTER TABLE `bet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_sell`
--
ALTER TABLE `buy_sell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edit_record`
--
ALTER TABLE `edit_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bet_status`
--
ALTER TABLE `bet_status`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `buy_sell`
--
ALTER TABLE `buy_sell`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `edit_record`
--
ALTER TABLE `edit_record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
