<?php

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $messageValue = getMessage($conn," WHERE uid = ? AND reply_message != '' ",array("uid"),array($uid),"s");

$soundfile = "dingdong.mp3";
$userMessageSoundValue = getMessage($conn," WHERE reply_three = ? ",array("reply_three"),array('NEW'),"s");
// $soundfile = "dingdong.mp3";
$userMessageValue = getMessage($conn," WHERE uid = ? AND reply_three = 'NEW' AND admin_status = 'REPLY' ",array("uid"),array($uid),"s");

              if($userMessageValue)
              {
              ?>

                <div class="blue-button text-center float-left profile-3-btn ow-blue-bg cs-btn">
                  <a href="viewMessage.php">
                    <?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?>

                        <?php
                        if($userMessageSoundValue)
                        {   
                            $totalUserMessageSoundValue = count($userMessageSoundValue);
                            // echo $totalMessageValue;
                            if($totalUserMessageSoundValue > 0)
                            {   
                                echo "<embed src =\"$soundfile\" hidden=\"true\" autostart=\"true\"></embed>";
                            }
                            else
                            {   }
                        }
                        ?>

                  </a>
                  <div class="red-dot blue-btn-red-dot"></div>
                </div>

              <?php
              }
              else
              {
              ?>

                <div class="blue-button text-center float-left profile-3-btn ow-blue-bg cs-btn">
                  <a href="viewMessage.php">
                    <?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?>
                  </a>
                </div>

              <?php
              }
              ?>