<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$totalForex = 0;

$conn = connDB();
$conn->close();
?>

  <table class="table-width data-table">
    <thead>
      <tr>
      <th><b><?php echo _USERDASHBOARD_INSTRUMENT ?></b></th>
	  <th><b><?php echo _USERDASHBOARD_AMOUNT ?></b></th>
      <th><b><?php echo _USERDASHBOARD_TIMELEFT ?></b></th>
      </tr>
    </thead>
    <tbody>

      <!-- <tr>
        <td id="newNumber"></td>
        <td ></td>
        <td ></td>
      </tr> -->
      <?php
      $conn = connDB();
      $betStatus = getBetstatus($conn, "WHERE uid = ?", array("uid"), array($_SESSION['uid']), "s");
      if ($betStatus)
      {
        for ($cnt=0; $cnt <count($betStatus) ; $cnt++)
        {
          $timeDate = $betStatus[$cnt]->getDateCreated();
          $timeline = $betStatus[$cnt]->getTimeline();
          $time = date("Y-m-d h:i:s a", strtotime($timeDate."+".$timeline."seconds"));
          $current = date("Y-m-d h:i:s a");
          if ($time > $current && ( strtotime($time) - strtotime($current)) >= 0)
          {
            ?>
            <tr>
              <td>
                <?php
                $currencyArrayEn = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");
                $currencyArrayCh = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");
                $currencyArrayEnExp = explode(",",$currencyArrayEn);
                $currencyArrayChExp = explode(",",$currencyArrayCh);
                for ($i=0; $i <count($currencyArrayEnExp) ; $i++) {
                  if($currencyArrayEnExp[$i] == $betStatus[$cnt]->getCurrency()){
                    if ( isset($_SESSION['lang']) && $_SESSION['lang'] == 'en' || isset($_GET['lang']) && $_GET['lang'] == 'en') {
                      echo $currencyArrayEnExp[$i];
                    }else {
                      echo $currencyArrayChExp[$i];
                    }

                  }
                }
                 ?>
              </td>
              <td>
                <?php echo "$ ".number_format($betStatus[$cnt]->getAmount()) ?>
              </td>
              <?php
              if (( strtotime($time) - strtotime($current)) < 10)
              {
              ?>
                <td style="color: red">
                  <?php echo ( strtotime($time) - strtotime($current))."s" ?>
                </td>
              <?php
              }
              else
              {
              ?>
                <td>
                  <?php echo ( strtotime($time) - strtotime($current))."s" ?>
                </td>
              <?php
              }
              ?>
            </tr>
            <?php
          }
        }
      }
      $conn->close();
      ?>
    </tbody>
  </table>
